# AIML-vlab

# Install required libraries
- pip install -r requirements.txt

# Make the script executable by running the following command in your terminal:
- Set-ExecutionPolicy -Scope Process -ExecutionPolicy Bypass

# Navigate to your root directory in the terminal and execute the script:
- ./server.sh
